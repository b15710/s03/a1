import java.util.ArrayList;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) {

        //games hashmap
        HashMap<String , Integer> games = new HashMap<String, Integer>();
        games.put("Mario Odyssey", 50);
        games.put("Super Smash Bros. Ultimate", 20);
        games.put("Luigi's Mansion 3", 15);
        games.put("Pokemon Sword", 30);
        games.put("Pokemon Shield", 100);

        games.forEach((key, value) -> {
            System.out.println(key + " has " + value + " stocks left.");
        });

        //Create an array list

        ArrayList<String> topGames = new ArrayList<>();
        
        games.forEach((key, value) -> {
            if (value <= 30) {
                topGames.add(key);
                System.out.println(key + " has been added to top games list!");
            }
        });

        System.out.println("Our shop's top games: " + "\n" + topGames);



            }


        }



